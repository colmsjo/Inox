@echo off

rem
rem Jonas C. 100214
rem
rem Changed from using ntbackup to EC2 snapshots. ntbackups takes a lot of time and CPU. 7z used for packaging takes even more time
rem
rem Arguments
rem %1 W1: week 1, W2 week 2, ...
rem used as part of file name in ntbackup and 7z.
rem Not used anymore with EC2 snapshots
rem

rem C:\WINDOWS\system32\ntbackup.exe backup "@C:\Documents and Settings\Administrator\Local Settings\Application Data\Microsoft\Windows NT\NTBackup\data\Full backup.bks" /n "Full backup set Week" /d "Full backup set Week" /v:yes /r:no /rs:no /hc:off /m normal /j "Full backup set Week" /l:s /f "C:\backups\Full-backup-set-%1.bkf"
rem del "C:\backups\Incremental-backup-set.bkf"
rem del "c:\backups\Full-backup-set-%1.7z"
rem c:\local\7-zip\7z a "c:\backups\Full-backup-set-%1.7z" "c:\backups\Full-backup-set-%1.bkf"
rem del "C:\backups\Full-backup-set-%1.bkf"
rem ftp -s:"c:\local\Gizur-tools\full-backup-set-%1.ftp"

rem Perform a EC2 sbapshot which can be used to re-create a volume
call c:\local\aws\snapshot-root-and-data.bat %1 s1.gizur.com

rem Also dump the DMS MySQL database
call c:\local\Gizur-tools\backup-mysql.bat

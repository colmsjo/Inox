@echo off
rem
rem Jonas C. 100214
rem
rem Using ntbackup, 7z and Nirvanix for remote storage of backup.
rem The method COPY is used, i.e. everything is backed up without setting any backup flag.
rem Run this monthly or manually. Should split data and programs/system state into separate backups.
rem
rem Arguments
rem %1 M1 - month 1, M2 month 2, ...
rem

rem -------------------------------- Program and System State
set SELECTION="@C:\Documents and Settings\Administrator\Local Settings\Application Data\Microsoft\Windows NT\NTBackup\data\Program-and-system-state.bks"
set BACKUP_NAME=Program-and-system-state-%1
set DESCRIPTION="Program and system state"

rem Run the bakcup
C:\WINDOWS\system32\ntbackup.exe backup %SELECTION% /n %DESCRIPTION% /d %DESCRIPTION% /v:yes /r:no /rs:no /hc:off /m copy /j %DESCRIPTION% /l:s /f "c:\backups\%BACKUP_NAME%.bkf"

rem Delete old packed backup file
del "c:\backups\%BACKUP_NAME%.7z"

rem Pack the new backup using 7z
c:\local\7-zip\7z a "c:\backups\%BACKUP_NAME%.7z" "c:\backups\%BACKUP_NAME%.bkf"

rem Delete the backup file in order to save space
del "C:\backups\%BACKUP_NAME%.bkf"


rem -------------------------------- Data
set SELECTION="@C:\Documents and Settings\Administrator\Local Settings\Application Data\Microsoft\Windows NT\NTBackup\data\data.bks"
set BACKUP_NAME=data-%1
set DESCRIPTION="Data"

rem Run the bakcup
C:\WINDOWS\system32\ntbackup.exe backup %SELECTION% /n %DESCRIPTION% /d %DESCRIPTION% /v:yes /r:no /rs:no /hc:off /m copy /j %DESCRIPTION% /l:s /f "c:\backups\%BACKUP_NAME%.bkf"

rem Delete old packed backup file
del "c:\backups\%BACKUP_NAME%.7z"

rem Pack the new backup using 7z
c:\local\7-zip\7z a "c:\backups\%BACKUP_NAME%.7z" "c:\backups\%BACKUP_NAME%.bkf"

rem Delete the backup file in order to save space
del "C:\backups\%BACKUP_NAME%.bkf"


rem -------------------------------- Store backup in remote storage
rem Store the backup in Nirvanix using ftp
rem ftp -s:"c:\local\Gizur-tools\full-backup-set-%1.ftp"
rem c:\local\wput\wput -awput-backup.log "c:\backups\data-%1.7z" ftp://jonas:homeend@colmsjo.dyndns.org/gizur2/tmp
rem c:\local\wput\wput -awput-backup.log "c:\backups\Program-and-system-state-%1.7z" ftp://jonas:homeend@colmsjo.dyndns.org/gizur2/tmp


call c:\local\Gizur-tools\s3backup.bat

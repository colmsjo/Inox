#!/bin/bash

NOW=$(date +"%y%m%d")

svnadmin dump  /var/www/svn/Inox > /backups/svn-Inox-${NOW}.dmp
gzip /backups/svn-Inox-${NOW}.dmp
/usr/bin/s3cmd put  /backups/svn-Inox-${NOW}.dmp.gz  s3://gizur-backups/`hostname`/
rm -f /backups/svn-Inox-${NOW}.dmp.gz

svnadmin dump  /var/www/svn/DigitalLocker > /backups/svn-DigitalLocker-${NOW}.dmp
gzip /backups/svn-DigitalLocker-${NOW}.dmp
/usr/bin/s3cmd put  /backups/svn-DigitalLocker-${NOW}.dmp.gz  s3://gizur-backups/`hostname`/
rm -f /backups/svn-DigitalLocker-${NOW}.dmp.gz

svnadmin dump  /var/www/svn/TradeServer > /backups/svn-TradeServer-${NOW}.dmp
gzip /backups/svn-TradeServer-${NOW}.dmp
/usr/bin/s3cmd put  /backups/svn-TradeServer-${NOW}.dmp.gz  s3://gizur-backups/`hostname`/
rm -f /backups/svn-TradeServer-${NOW}.dmp.gz





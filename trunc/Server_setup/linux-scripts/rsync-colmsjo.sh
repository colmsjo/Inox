#!/bin/bash
#
# 100718 Jonas C.
#
# 100830 - Converted from bat file to linux shell script
#
# This script assumes that secure ssh login on the remote host has been setup
# 1. on local: ssh-keygen -t dsa
# 2. on local: scp id_sda.pub user@remote:/...
# 3. on remote: cat id_dsa >> ~/.ssh/authorized_keys2
# 4. on local: test to login with ssh user@remote, you should not have to enter the password now
#

#
# Setup variabels
#
CMD="rsync -rpcztP --bwlimit=500"
#RUN_LOG="/root/log/batchrun.log"
#ERR_LOG="/root/log/batchrun.err"
#LOG_CMD="echo `date`:"
TARGET="Administrator@s1.gizur.com:/cygdrive/c/data/colmsjo"

# Copy 5.Jonas
SRC="/mnt/linksys_colmsjo/5.Jonas"
_CMD="$CMD $SRC $TARGET"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/rsync-$PPID.log 2>> $ERR_LOG

# Copy 6.Gemensamt
SRC="/mnt/linksys_colmsjo/6.Gemensamt"
_CMD="$CMD $SRC $TARGET"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/rsync-$PPID.log 2>> $ERR_LOG

# Copy 7.Linda 
SRC="/mnt/linksys_colmsjo/7.Linda"
_CMD="$CMD $SRC $TARGET"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/rsync-$PPID.log 2>> $ERR_LOG

# Copy 8.Photo 
SRC="/mnt/linksys_colmsjo/8.Photo"
_CMD="$CMD $SRC $TARGET"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/rsync-$PPID.log 2>> $ERR_LOG

#!/bin/sh

# Needed if not run by root
# PATH=$PATH:/usr/sbin:/sbin

export SDL_VIDEO_X11_DGAMOUSE=0

#
# Add tap interfaces
#

sudo tunctl -b -u  root
sudo ifconfig tap0 up
sudo brctl addif br0 tap0

sudo tunctl -b -u  root
sudo ifconfig tap1 up
sudo brctl addif br0 tap1

sudo tunctl -b -u  root
sudo ifconfig tap2 up
sudo brctl addif br0 tap2

sudo tunctl -b -u  root
sudo ifconfig tap3 up
sudo brctl addif br0 tap3

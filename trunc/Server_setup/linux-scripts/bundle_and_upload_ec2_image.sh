#!/bin/bash
#
# Jonas C. 101115 
#
# Script for taking an image of a virtual machine and upload in to Amazon S3.
# The image is bundled and registered as well.
#

if [ ! -n "$1" ]
then
  echo "Usage: `basename $0` directory image name"
  exit -1 
fi

if [ ! -n "$2" ]
then
  echo "Usage: `basename $0` directory image name"
  exit -1
fi

if [ ! -n "$3" ]
then
  echo "Usage: `basename $0` directory image name"
  exit -1
fi

old=$(pwd)

bundleimage -u 4290-9734-6310 -c $EC2_CERT -k $EC2_PRIVATE_KEY -i $1/$2 -d /vm/bundled/ -r i386 > ~/log/bundle_and_upload.log

ec2-upload-bundle -b gizur/$2 -a $(cat /etc/ec2/access_key) -s $(cat /etc/ec2/secret_access_key) -m /vm/bundled/$2.manifest.xml --location EU --retry   >> ~/log/bundle_and_upload.log

ec2-register -n $3 gizur/$2/$2.manifest.xml  >> ~/log/bundle_and_upload.log

rm -f /vm/bundled/*  >> ~/log/bundle_and_upload.log

cd $old


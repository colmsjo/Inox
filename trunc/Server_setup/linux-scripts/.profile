export JAVA_HOME=/usr/java/jdk1.6.0_21/
export PATH=$PATH:${JAVA_HOME}bin

export DISPLAY=localhost:1

export EDITOR=/bin/vi
export ADEMPIERE_HOME=/opt/Adempiere

export EC2_HOME=/usr/share/ec2-api-tools
export PATH=$PATH:$EC2_HOME/bin
export EC2_PRIVATE_KEY=/etc/ec2/private-key.pem
export EC2_CERT=/etc/ec2/cert.pem
export EC2_URL=https://ec2.eu-west-1.amazonaws.com

export RUN_LOG="/root/log/batchrun.log"
export ERR_LOG="/root/log/batchrun.err"
export LOG_CMD="echo `date`:$@:"

#!/bin/bash

source /root/.profile

$LOG_CMD "s3backup_wrapper.sh" >> $RUN_LOG 

# Get week number and use mod 3 which batch to run 

BACK_SET=$(expr `date +"%U"` % 3)

#
# Focus on the colmsjo volume for a while - photos still not uploaded
#
/root/scripts/s3backup_colmsjo.sh
 
case $BACK_SET in
       0)    /root/scripts/s3backup_backups.sh;;
       1)    /root/scripts/s3backup_colmsjo.sh;;
       *)    /root/scripts/s3backup_gizur-docs.sh;;
esac > /root/log/s3backup_wrapper.log 2>&1

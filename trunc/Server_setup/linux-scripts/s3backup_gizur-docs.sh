#!/bin/bash
#
# 100718 Jonas C.
#

source /root/.profile

$LOG_CMD "s3backup_gizur-docs.sh" >> $RUN_LOG

export DISPLAY=localhost:1

cd /opt/BucketExplorer/

/opt/BucketExplorer/BucketCommander.sh -action:upload -authenticate:gizur_s3 -configfilename:"/root/scripts/BucketCommander.xml" -bucketname:gizur-docs &> /root/log/bucketcommander-gizur-docs.log &

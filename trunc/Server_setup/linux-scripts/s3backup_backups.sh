#!/bin/bash
#
# 100718 Jonas C.
#

source /root/.profile

#
# Write entry to log-file using macrco defined in .profile
#

$LOG_CMD "s3backup_backups.sh" >> $RUN_LOG

#export DISPLAY=localhost:1

cd /opt/BucketExplorer/

/opt/BucketExplorer/BucketCommander.sh -action:upload -authenticate:gizur_s3 -configfilename:"/root/scripts/BucketCommander-backups.xml" -bucketname:gizur-backups &> /root/log/bucketcommander-backups.log &

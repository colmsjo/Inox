#!/bin/bash
#
# 101029 Jonas C.
#
# Use s3sync ruby scrips to sync data to Amazon S3 buckets
#

. /root/.bashrc

/root/s3sync/s3sync.rb /mnt/linksys_colmsjo/8.Photo/ linksys_colmsjo:/8.Photo > /root/s3sync-colmsjo.log &


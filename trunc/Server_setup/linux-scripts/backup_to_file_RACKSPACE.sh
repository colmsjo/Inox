#!/bin/bash
# A UNIX / Linux shell script to backup dirs to tape device like /dev/st0 (linux)
# This script make both full and incremental backups.
# You need at two sets of five  tapes. Label each tape as Mon, Tue, Wed, Thu and Fri.
# You can run script at midnight or early morning each day using cronjons.
# The operator or sys admin can replace the tape every day after the script has done.
# Script must run as root or configure permission via sudo.
# -------------------------------------------------------------------------
# Copyright (c) 1999 Vivek Gite <vivek@nixcraft.com>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of nixCraft shell script collection (NSSC)
# Visit http://bash.cyberciti.biz/ for more information.
# -------------------------------------------------------------------------
# Last updated on : March-2003 - Added log file support.
# Last updated on : Feb-2007 - Added support for excluding files / dirs.
# -------------------------------------------------------------------------

#
# Log to batchrun.log
#
#$LOG_CMD "backup_to_file" >> $RUN_LOG

# Must be set in order to create separate files for the machin
MACHINE="`hostname`"
# "gizur-server2"

LOGBASE=/root/backup/log
 
# Backup dirs; do not prefix /
BACKUP_ROOT_DIR="bin dev etc home lib lib64 media opt proc root sbin selinux srv sys usr var"
 
# Get todays day like Mon, Tue and so on
NOW=$(date +"%a")

# Get week number and use mod 4 to create 4 backup set that are rotated monthly
BACK_SET=$(expr `date +"%U"` % 4)
 
# Rotate backup every four weeks
TAPE_WEEKLY="/backups/backup-set${BACK_SET}.tar"

# Incremental daily backups that are rotated weekly. Mondays are full backups. 
TAPE="/backups/backup-${NOW}.tar"
 
# Exclude file
TAR_ARGS=""
EXCLUDE_CONF=/root/.backup.exclude.conf
 
# Backup Log file
LOGFIILE=$LOGBASE/$NOW.backup.log
 
# Path to binaries
TAR=/bin/tar
MT=/bin/mt
MKDIR=/bin/mkdir
 
# ------------------------------------------------------------------------
# Excluding files when using tar
# Create a file called $EXCLUDE_CONF using a text editor
# Add files matching patterns such as follows (regex allowed):
# home/vivek/iso
# home/vivek/*.cpp~
# ------------------------------------------------------------------------
[ -f $EXCLUDE_CONF ] && TAR_ARGS="-X $EXCLUDE_CONF"
 
#### Custom functions #####
# Make a full backup
full_backup(){
        local old=$(pwd)
        cd /
        $TAR $TAR_ARGS -czvpf $TAPE $BACKUP_ROOT_DIR
        gzip -f $TAPE
        cp $TAPE.gz $TAPE_WEEKLY.gz 
        /usr/bin/s3cmd put /backups/{$TAPE_WEEKLY}.gz s3://gizur-backups/gizur-server2 
        cd $old
}
 
# Make a  partial backup
partial_backup(){
        local old=$(pwd)
        cd /

        # Running daily backups, change to weekly by switching line
        # $TAR $TAR_ARGS -czvpf $TAPE -N "$(date -d '1 week ago')" $BACKUP_ROOT_DIR
        $TAR $TAR_ARGS -czvpf $TAPE -N "$(date -d '1 day ago')" $BACKUP_ROOT_DIR
        gzip -f $TAPE

        /usr/bin/s3cmd put /backups/{$TAPE}.gz s3://gizur-backups/gizur-server2

        cd $old
}
 
# Make sure all dirs exits
verify_backup_dirs(){
        local s=0
        for d in $BACKUP_ROOT_DIR
        do
                if [ ! -d /$d ];
                then
                        echo "Error : /$d directory does not exits!"
                        s=1
                fi
        done
        # if not; just die
        [ $s -eq 1 ] && exit 1
}
 
#### Main logic ####
 
# Make sure log dir exits
[ ! -d $LOGBASE ] && $MKDIR -p $LOGBASE
 
# Verify dirs
verify_backup_dirs
 
# Okay let us start backup procedure
# If it is monday make a full backup;
# For Tue to Fri make a partial backup
# Weekend no backups
# Okay let us start backup procedure
# If it is monday make a full backup;
# For Tue to Fri make a partial backup
# Weekend no backups
case $NOW in
        Mon)    full_backup;;
        Tue|Wed|Thu|Fri|Sat|Sun)        partial_backup;;
        *) ;;
esac > $LOGFIILE 2>&1

# Alterntive for weekly backups
# case $BACK_SET in
#        0)    full_backup;;
#        1|2|3)    partial_backup;;
#        *) ;;
# esac > $LOGFIILE 2>&1

#!/bin/sh

# Needed if not run by root
# PATH=$PATH:/usr/sbin:/sbin

#
# Create bridge
#

sudo brctl addbr br0
sudo ifconfig eth0 0.0.0.0
sudo brctl addif br0 eth0
sudo ifconfig br0 192.168.1.100 netmask 255.255.255.0 up
sudo route add -net 192.168.1.0 netmask 255.255.255.0 br0
sudo route add default gw 192.168.1.1 br0

#
# Add bridge to the firewall
#

# sudo iptables -I RH-Firewall-1-INPUT -i br0 -j ACCEPT


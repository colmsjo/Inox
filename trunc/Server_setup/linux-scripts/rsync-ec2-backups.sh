#!/bin/bash

#
# 100718 Jonas C.
#
# 100830 - Converted from bat file to linux shell script
# 100909 - Changed and started using variables in order to simplify maintenance


#
# Setup variabels
#
CMD="rsync -rpcztP --bwlimit=3000"
#RUN_LOG="/root/log/batchrun.log"
#ERR_LOG="/root/log/batchrun.err"
#LOG_CMD="echo `date`:"
TARGET="/mnt/linksys_backups/gizur-server-1"

# Copy gizur1/Dokument
SRC="Administrator@s1.gizur.com:/cygdrive/c/backups"
_CMD="$CMD $SRC $TARGET"
$LOG_CMD "$_CMD" >> $RUN_LOG
#$_CMD > /root/log/rsync-$PPID.log 2>> $ERR_LOG

$_CMD



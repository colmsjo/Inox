#!/bin/bash

EXPECTED_ARGS=2
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` file path"
  exit $E_BADARGS
fi


if [ -e $2$1 ]
then
  echo "File exists, no need to add to svn..." 
  cp $1 $2
  svn commit -m "commiting file..." $2$1
else
  cp $1 $2
  svn add $2$1
  svn commit -m "commiting file new..." $2$1
fi


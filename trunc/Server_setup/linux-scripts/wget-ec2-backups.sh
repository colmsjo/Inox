#!/bin/bash

#
# 100718 Jonas C.
#
# 100830 - Converted from bat file to linux shell script
# 100909 - Changed and started using variables in order to simplify maintenance


#
# Setup variabels
#
CMD="wget --http-user=jonas --http-password=homeend "
RUN_LOG="/root/log/batchrun.log"
ERR_LOG="/root/log/batchrun.err"
LOG_CMD="echo `date`:"
TARGET="/mnt/linksys_backups/gizur-server-1"

# Copy W1 
SRC="http://s1.gizur.com/backups/Program-and-system-state-W1.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/Program-and-system-state-W1.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log 

SRC="http://s1.gizur.com/backups/data-W1.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/data-W1.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log


# Copy W2
SRC="http://s1.gizur.com/backups/Program-and-system-state-W2.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/Program-and-system-state-W2.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log

SRC="http://s1.gizur.com/backups/data-W2.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/data-W2.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log


# Copy W3
SRC="http://s1.gizur.com/backups/Program-and-system-state-W3.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/Program-and-system-state-W3.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log

SRC="http://s1.gizur.com/backups/data-W3.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/data-W3.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log

# Copy W4
SRC="http://s1.gizur.com/backups/Program-and-system-state-W4.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/Program-and-system-state-W4.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log

SRC="http://s1.gizur.com/backups/data-W4.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/data-W4.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log


# Copy M1
SRC="http://s1.gizur.com/backups/Program-and-system-state-M1.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/Program-and-system-state-M1.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log

SRC="http://s1.gizur.com/backups/data-M1.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/data-M1.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log

# Copy M2
SRC="http://s1.gizur.com/backups/Program-and-system-state-M2.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/Program-and-system-state-M2.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log

SRC="http://s1.gizur.com/backups/data-M2.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/data-M2.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log


# Copy M3
SRC="http://s1.gizur.com/backups/Program-and-system-state-M3.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/Program-and-system-state-M3.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log

SRC="http://s1.gizur.com/backups/data-M3.7z"
TARGET="/mnt/linksys_backups/gizur-server-1/data-M3.7z"
_CMD="$CMD -O $TARGET $SRC"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/wget-$PPID.log 2>  /root/log/wget-$PPID.log

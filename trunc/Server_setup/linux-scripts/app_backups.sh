#!/bin/bash
#
# Runs backups on the application level. Often dumps from the database, MySQl etc.
# Backups are stored off-site in Amazon S3 storage
#
# -------------------------------------------------------------------------
# Copyright (c) 20110503 Jonas ColmsjöA 
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------

NOW=$(date +"%y%m%d")

#
# Knowlwdgetree
#

mysqldump dms -udmsadmin -pjs9281djw > /backups/knowledgetree-${NOW}.sql
gzip /backups/knowledgetree-${NOW}.sql
/usr/bin/s3cmd put  /backups/knowledgetree-${NOW}.sql.gz  s3://gizur-backups/`hostname`/ 


#
# vTiger
#

mysqldump vtigercrm520 -uroot -phomeend > /backups/vtigercrm-${NOW}.sql
gzip /backups/vtigercrm-${NOW}.sql
/usr/bin/s3cmd put  /backups/vtigercrm-${NOW}.sql.gz  s3://gizur-backups/`hostname`/


#
# Mediawiki
#

/usr/bin/php /var/www/html/mediawiki/maintenance/dumpBackup.php  --full > /backups/mediawiki-${NOW}.dmp
gzip /backups/mediawiki-${NOW}.dmp
/usr/bin/s3cmd put /backups/mediawiki-${NOW}.dmp.gz  s3://gizur-backups/`hostname`/ 


#!/bin/bash
#
# 100718 Jonas C.
#
# 100830 - Converted from bat file to linux shell script
#
# This script assumes that secure ssh login on the remote host has been setup
# 1. on local: ssh-keygen -t dsa
# 2. on local: scp id_sda.pub user@remote:/...
# 3. on remote: cat id_dsa >> ~/.ssh/authorized_keys2
# 4. on local: test to login with ssh user@remote, you should not have to enter the password now
#

#
# Setup variabels
#
CMD="rsync -rpcztP --bwlimit=500"
#RUN_LOG="/root/log/batchrun.log"
#ERR_LOG="/root/log/batchrun.err"
#LOG_CMD="echo `date`:"
TARGET="Administrator@s1.gizur.com:/cygdrive/c/data/colmsjo"

# Copy gizur1/Dokument 
SRC="/mnt/linksys_gizur1/Dokument"
_CMD="$CMD $SRC $TARGET"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/rsync-$PPID.log 2>> $ERR_LOG


# Copy  gizur2/Resources
SRC="/mnt/linksys_gizur2/Resources"
_CMD="$CMD $SRC $TARGET"
$LOG_CMD "$_CMD" >> $RUN_LOG
$_CMD > /root/log/rsync-$PPID.log 2>> $ERR_LOG
